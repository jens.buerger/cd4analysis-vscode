#################################################################
## Iro
################################################################ 
##
## * Press Ctrl + '+'/'-' To Zoom in
## * Press Ctrl + S to save and recalculate... 
## * Documents are saved to web storage.
## * Only one save slot supported.
## * Matches cannot span lines.
## * Unicode chars must be defined in \u0000 to \uffff format.
## * All matches must be contained by a single group ( ... )
## * Look behinds not permitted, (?<= or (?<!
## * Look forwards are permitted (?= or (?!
## * Constants are defined as __my_const = (......)
## * The \= format allows unescaped regular expressions
## * Constants referenced by match \= $${__my_const}
## * Constants can reference other constants
## * You are free to delete all the default scopes.
## * Twitter : ainslec , Web: http://eeyo.io/iro
##
################################################################

name                   = cd4analysis
file_extensions []     = cd;

################################################################
## Constants
################################################################

__MY_CONSTANT \= (\b[a-z][a-z0-9]*)
__MY_CLASS_NAME \= (\b[$A-Z][A-Za-z0-9_$]*)
__MY_ASSOCIATION_NAME \= (\b[a-z][A-Za-z0-9_$]*)

################################################################
## Styles
################################################################

styles [] {

.comment : style {
   color                 = light_green
   italic                = true
   ace_scope             = comment
   textmate_scope        = comment
   pygments_scope        = Comment
}

.keyword : style {
   color     = orange
   ace_scope             = keyword
   textmate_scope        = keyword
   pygments_scope        = Keyword
}

.numeric : style {
   color                 = gold
   ace_scope             = constant.numeric
   textmate_scope        = constant.numeric
   pygments_scope        = Number
}

.cardinality : style {
   color                 = gold
   ace_scope             = constant.numeric
   textmate_scope        = constant.numeric
   pygments_scope        = Number
}


.punctuation : style {
   color                 = white
   ace_scope             = punctuation
   textmate_scope        = punctuation
   pygments_scope        = Punctuation
}

.text : style {
   color                 = brown
   ace_scope             = text
   textmate_scope        = string.quoted.double
   pygments_scope        = String
}

.char : style {
   color                 = brown
   ace_scope             = text
   textmate_scope        = constant.character
   pygments_scope        = String
}



.associationname : style {
   color                 = purple
   ace_scope             = text
   textmate_scope        = entity.name.function
   pygments_scope        = String
}

.rolename : style {
   color                 = cyan
   ace_scope             = text
   textmate_scope        = entity.name.tag
   pygments_scope        = String
}

.classref : style {
   color                 = purple
   ace_scope             = text
   textmate_scope        = entity.other.inherited-class
   pygments_scope        = String
}

.classname : style {
   color                 = yellow
   ace_scope             = text
   textmate_scope        = entity.name.type
   pygments_scope        = String
}


.langconstant : style {
   color                 = red
   ace_scope             = text
   textmate_scope        = constant.language
   pygments_scope        = String
}

.java : style {
   color                 = white
   ace_scope             = invalid
   textmate_scope        = meta.embedded.block.java
   pygments_scope        = Generic.Error
}

.illegal : style {
   color                 = white
   background_color      = red
   ace_scope             = invalid
   textmate_scope        = invalid
   pygments_scope        = Generic.Error
}

}

#################################################
## Parse contexts
#################################################

contexts [] {

##############################################
## Main Context - Entry point context
##############################################

main : context {
   default_style      = .punctuation
   : pattern {
      description        = keywords outside of grammar definition
      regex          \= (\b(package|import|classdiagram)\b)
      styles []       = .keyword;
   }
   
   : include "mcliteral" ;
   
   : inline_push {
      regex          \= (\{)
      styles []       = .punctuation;
      : pop {  
         regex       \= (\})
         styles []    = .punctuation;
      }
      : include "classdiagram" ;
      // todo maybe improve entry role for grammarbody
   }
   
   : pattern {
      regex          \= (;)
      styles []       = .punctuation;
   }
   
   
   : include "comments" ;

   
}


###########################################
## ClassDiagram Context
###########################################

classdiagram : context {

   : inline_push {
      regex          \= (association|composition)
      styles []       = .keyword;
      : pop {  
         regex       \= (;)
         styles []    = .punctuation;
      }
      : include "association" ;
   }
   
   : inline_push {
      regex          \= (\()
      styles []       = .punctuation;
      : pop {
         regex       \= (\))
         styles []    = .punctuation;
      }
      : include "classdiagram" ;
   }
   
   : inline_push {
      regex          \= (\[)
      styles []       = .punctuation;
      : pop {
         regex       \= (\])
         styles []    = .punctuation;
      }
      : include "classdiagram" ;
   }
   
   : include "javaspec" ;
   // TODO: bug javainline still has precedence
   : include "javainline" ;
   
   // just for iro development
   // : inline_push {
   //    regex          \= (\{)
   //    styles []       = .punctuation;
   //    : pop {  
   //       regex       \= (\})
   //       styles []    = .punctuation;
   //    }
   //    : include "javainline" ;
   // }
   
   : include "comments" ;
   

   // : pattern {
   //    regex          \= ([^\s])
   //    styles []       = .keyword;
   // }
   
}


#################################################
## Association Context
#################################################

association : context {
   : pattern {
      description = association direction
      regex          \= (<->|->|<-|--)
      styles []       = .keyword;
   }
   
   : include "comments" ;
   
   : pattern {
      regex          \= $${__MY_ASSOCIATION_NAME}
      styles []       = .associationname;
   }
   
   : pattern {
      regex          \= $${__MY_CLASS_NAME}
      styles []       = .classref;
   }
   
   : inline_push {
      regex          \= (\[\[)
      default_style  = .rolename 
      styles []       = .punctuation;
      : pop {  
         regex       \= (\]\])
         styles []    = .punctuation;
      }
   }
   
   : inline_push {
      regex          \= (\[)
      styles []       = .punctuation;
      : pop {  
         regex       \= (\])
         styles []    = .punctuation;
      }
      : include "cardinality" ;
   }
   
   : inline_push {
      regex          \= (\()
      default_style  = .rolename 
      styles []       = .punctuation;
      : pop {  
         regex       \= (\))
         styles []    = .punctuation;
      }
   }
   
  
   : pattern {
      regex          \= (.)
      styles []       = .punctuation;
   }
   
}


#################################################
## AssociationCardinality Context
#################################################

cardinality : context {
   : inline_push {
      regex          \= (\{)
      styles []       = .punctuation;
      : pop {  
         regex       \= (\})
         styles []    = .punctuation;
      }
      : include "javainline" ;
   }
   
   : pattern {
      description = association direction
      regex          \= ([0-9\*])
      styles []       = .cardinality;
   }
  
   : pattern {
      regex          \= (\.\.)
      styles []       = .punctuation;
   }
   
}



#################################################
## Comment Context
#################################################

comments : context {
   description        = all comment types
   : inline_push {
      regex          \= (/\*)
      styles []       = .comment;
      default_style   = .comment
      : pop {
         regex       \= (\*/)
         styles []    = .comment;
      }
   }
   
   : pattern {
      regex          \= (//.*)
      styles []       = .comment;
   }
   
   : inline_push {
      regex          \= (<<)
      default_style  = .punctuation 
      styles []       = .punctuation;
      : pop {  
         regex       \= (>>)
         styles []    = .punctuation;
      }
   }
   
   // : pattern {
   //    regex          \= ([^\s])
   //    styles []       = .illegal;
   // }
}

###########################################
## MC Literals Context
###########################################

mcliteral : context {
   : pattern {
      regex          \= (\b\d+)
      styles []       = .numeric;
   }
   
   : inline_push {
      regex          \= (\")
      styles []       = .text;
      default_style   = .text
      : pop {
         regex       \= (\")
         styles []    = .text;
      }
   }
   
   : inline_push {
      regex          \= (\')
      styles []       = .char;
      default_style   = .char
      : pop {
         regex       \= (\')
         styles []    = .char;
      }
   }
}

###########################################
## javaspec Context
###########################################

javaspec : context {
   : pattern {
      description = short java type type keyword
      regex          \= ((?:interface|class|abstract)(?=[^\{\}]*;))
      styles []       = .keyword;
   }
   
   : pattern {
      description = short java type inheritence keyword
      regex          \= ((?:implements|extends)(?=[^\{\}]*;))
      styles []       = .keyword;
   }
   
   : pattern {
      description = short java type ref
      regex          \= ((?<=implements|extends)\s+[\S_]*(?=[^\{\}]*;))
      styles []       = .classref;
   }
   
   : pattern {
      description = short java type ref
      regex          \= ((?<=class|interface)\s+[\S_]*(?=[^\{\}]*;))
      styles []       = .classname;
   }
}


###########################################
## Java Context
###########################################
// !!!!! After the generation replace the include with source.java !!!!
javainline : context {
   default_style      = .java
   : include "mcliteral" ;
}

}
