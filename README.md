# CD4Analysis VSCode

This VSCode extension provides language support for CD4Analysis (cd) files.

## Features

Currently the extension provides the following features:

* syntax highlighting
* syntax highlighting of inline Java code
* snippets


## Known Issues

* Class/Interface Definitions without braces are not properly supported since we mainly reuse the Java grammar
* Currently no language server is used.

## Release Notes

This extension is currently in beta

-----------------------------------------------------------------------------------------------------------
